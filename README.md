# Integra tu chatbot de Dialogflow a Facebook  Messenger

Aquí se muestra como integrar un chatbot de dialogflow a la plataforma Facebook Messenger. 


# Requisitos 

* Tener un agente que se desee integrar a facebook messenger. 
* Una cuenta de facebook.
* Una pagina de facebook a la cual agregar su agente. 


# Tutorial 

Debera ingresar a la consola de desarrollador de facebook y crear una aplicacion nueva. 

 ![](imagenes/1.png)

Luego de dar clic en **Create App ID**  

Regresamos al panel principal en el cual seleccionaremos la opcion **Configurar** de la aplicacion **Messenger**. 

 ![](imagenes/2.png)

Despues en la seccion **Token Generation** debera elegir la pagina previamente creada en facebook y automaticamente se genera un **Page Access Token**

 ![](imagenes/3.png)



### Configuracion de dialogflow

En la opcion integraciones de dialogflow, activar la opcion Facebook Messenger

 ![](imagenes/4.png)


El **Token de verificacion** puede utilizar cualquier cadena de caracteres. 

El **Page Access Token** Ingresaremos el token generado anteriormente en la consola de desarrollador de facebook. 

Finalmente daremos clic en START 

![](imagenes/token.png)

### Configuracion de webhook 

Es el ultimo paso para configurar su agente en facebook messenger. 

En la consola de desarrollador de Facebook seleccionar **Configurar Webhooks**

 ![](imagenes/5.png)


En URL de devolucion de llamada ingresar la URL proporcionada por dialogflow. En verificar token ingresar su token de verificacion previamnete configurados. 

Finalmente dar clic en Verificar y guardar. 

## Test 

Finalmente para que pueda probar el bot su aplicacion debera ser publica. 

Para esto debera seleccionar App Review y seleccionar su app para hacerla publica.

Luego debera seleccionar la categoria "Apps para Messenger", finalmente confirmar. 

![](imagenes/7.png)
